# vim: filetype=yaml sw=2
filename: 'container-image_[% c("var/container/suite") %]-[% c("var/container/arch") %]-[% c("version") %].tar.gz'
version: 2
pkg_type: build

var:
  ubuntu_version: 19.10

  container:
    use_container: 1
    # We need CAP_SYS_ADMIN for debootstrap to work
    CAP_SYS_ADMIN: 1

pre: |
  #!/bin/sh
  set -e
  export DEBIAN_FRONTEND=noninteractive
  apt-get update -y -q
  apt-get install -y -q debian-archive-keyring ubuntu-keyring debootstrap
  debootstrap --arch=[% c("var/container/arch") %] [% c("var/container/debootstrap_opt") %] [% c("var/container/suite") %] base-image [% c("var/container/debootstrap_mirror") %]
  [% IF c("var/apt_package_filename") || c("var/apt_utils_package_filename") || c("var/libapt_inst_package_filename") || c("var/libapt_pkg_package_filename") -%]
    mkdir ./base-image/apt-update
    mv [% c("var/apt_package_filename") %] [% c("var/apt_utils_package_filename") %] \
       [% c("var/libapt_inst_package_filename") %] [% c("var/libapt_pkg_package_filename") %] \
       ./base-image/apt-update
    mount proc ./base-image/proc -t proc
    mount sysfs ./base-image/sys -t sysfs
    chroot ./base-image dpkg -i -R /apt-update
    umount ./base-image/proc
    umount ./base-image/sys
  [% END -%]
  [% IF c("var/minimal_apt_version") -%]
    apt_version=$(dpkg --admindir=$(pwd)/base-image/var/lib/dpkg -s apt | grep '^Version: ' | cut -d ' ' -f 2)
    echo "apt version: $apt_version"
    dpkg --compare-versions "$apt_version" ge '[% c("var/minimal_apt_version") %]'
  [% END -%]
  tar -C ./base-image -czf [% dest_dir %]/[% c("filename") %] .

targets:
  wheezy-amd64:
    var:
      minimal_apt_version: '0.9.7.9+deb7u9'
      # https://deb.freexian.com/extended-lts/updates/ela-76-1-apt/
      apt_packages_baseurl: http://deb.freexian.com/extended-lts/pool/main/a/apt
      apt_package_filename: apt_0.9.7.9+deb7u9_amd64.deb
      apt_package_sha256sum: b8a218da2ae21979323dd02551d983938d94308ac56930a2d238e822f062dc61
      apt_utils_package_filename: apt-utils_0.9.7.9+deb7u9_amd64.deb
      apt_utils_package_sha256sum: d635a166aae03b24cdabe277bb46d584156836637c0b9bef2220fb0bc0928e40
      libapt_inst_package_filename: libapt-inst1.5_0.9.7.9+deb7u9_amd64.deb
      libapt_inst_package_sha256sum: 332083172e00d39e55dec4c7078c3522989e45e12b0a1306889e90b8e2c0a0db
      libapt_pkg_package_filename: libapt-pkg4.12_0.9.7.9+deb7u9_amd64.deb
      libapt_pkg_package_sha256sum: c3ae83c4ea691074250bf6fb175e71042c4bb7418bf19779ceda0a53eea0a257

      container:
        suite: wheezy
        arch: amd64
        debootstrap_mirror: "http://archive.debian.org/debian/"

  stretch-amd64:
    var:
      minimal_apt_version: 1.4.9
      container:
        suite: stretch
        arch: amd64

  buster-amd64:
    var:
      minimal_apt_version: 1.8.2
      container:
        suite: buster
        arch: amd64

input_files:
  - URL: 'http://cdimage.ubuntu.com/ubuntu-base/releases/[% c("var/ubuntu_version") %]/release/ubuntu-base-[% c("var/ubuntu_version") %]-base-amd64.tar.gz'
    filename: 'container-image_ubuntu-base-[% c("var/ubuntu_version") %]-base-amd64.tar.gz'
    sha256sum: eedcb1dc0ccc86b59eb1f89960c322a2ba3ed3e0323a20a1da8bcc0e6f100f4f
  - URL: '[% c("var/apt_packages_baseurl") %]/[% c("var/apt_package_filename") %]'
    sha256sum: '[% c("var/apt_package_sha256sum") %]'
    enable: '[% c("var/apt_package_filename") %]'
  - URL: '[% c("var/apt_packages_baseurl") %]/[% c("var/apt_utils_package_filename") %]'
    sha256sum: '[% c("var/apt_utils_package_sha256sum") %]'
    enable: '[% c("var/apt_utils_package_filename") %]'
  - URL: '[% c("var/apt_packages_baseurl") %]/[% c("var/libapt_inst_package_filename") %]'
    sha256sum: '[% c("var/libapt_inst_package_sha256sum") %]'
    enable: '[% c("var/libapt_inst_package_filename") %]'
  - URL: '[% c("var/apt_packages_baseurl") %]/[% c("var/libapt_pkg_package_filename") %]'
    sha256sum: '[% c("var/libapt_pkg_package_sha256sum") %]'
    enable: '[% c("var/libapt_pkg_package_filename") %]'
